//
//  File.swift
//  RedditAPI
//
//  Created by Christian Hipolito on 7/26/19.
//  Copyright © 2019 Christian Hipolito. All rights reserved.
//

import UIKit
import SnapKit
import Alamofire
import AlamofireImage

protocol PopularPostsCellDelegate: class {
    func refresh(_ cell: PopularPostsCell)
}

final class PopularPostsCell: UITableViewCell
{
    weak var delegate: PopularPostsCellDelegate?
    
    lazy var title: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.font = UIFont(name: "Helvetica-Bold", size: 14)
        
        self.contentView.addSubview(label)
        
        label.snp.makeConstraints{ (constraintMaker) in
            constraintMaker.leading.equalToSuperview().offset(10)
            constraintMaker.trailing.equalToSuperview().offset(-10)
            constraintMaker.top.equalToSuperview().offset(10)
        }
        
        return label
    }()
    
    lazy var postImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        
        self.contentView.addSubview(imageView)
        
        imageView.snp.makeConstraints{(constraintMaker) in
            constraintMaker.top.equalTo(self.title.snp.bottom).offset(12)
            constraintMaker.leading.equalToSuperview()
            constraintMaker.trailing.equalToSuperview()
            constraintMaker.bottom.equalToSuperview().offset(-10)
        }
        
        return imageView
    }()
    
    func setup(with post: Post) {
        title.text = post.title
        postImageView.image = nil
        
        
        if let url = post.imageURL {
            postImageView.af_setImage(withURL: url) {[weak self] (response) in
                guard let self = self else {
                    return
                }
                
                self.delegate?.refresh(self)
            }
        }
    }
}
