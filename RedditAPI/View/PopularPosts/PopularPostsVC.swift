//
//  ViewController.swift
//  RedditAPI
//
//  Created by Christian Hipolito on 7/25/19.
//  Copyright © 2019 Christian Hipolito. All rights reserved.
//

import UIKit
import PromiseKit

final class PopularPostsVC: UITableViewController, PopularPostsCellDelegate
{
    private let viewModel = PopularPostsVM()
    let idPopularPostCell = "idPopularPostCell"
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        setupView()
        fetchPosts()
    }


    private func setupView()
    {
        self.tableView.register(PopularPostsCell.self, forCellReuseIdentifier: idPopularPostCell)
        self.tableView.rowHeight = UITableView.automaticDimension
    }
    
    private func fetchPosts()
    {
        firstly {
            viewModel.fetchPosts()
        }.done {
            self.tableView.reloadData()
        }.catch { (error) in
            self.hadle(error: error)
        }
    }
    
    private func hadle(error:Error)
    {
        let okAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        
        let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
        
        alert.addAction(okAction)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.posts.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: idPopularPostCell, for: indexPath) as! PopularPostsCell
        cell.delegate = self
    
        let post = viewModel.posts[indexPath.row]
        
        cell.setup(with: post)
        
        return cell
    }
    
    func refresh(_ cell: PopularPostsCell)
    {
        guard let indexPath = tableView.indexPath(for: cell) else {
            return
        }
        
        self.tableView.reloadRows(at: [indexPath], with: .none)
    }
}

