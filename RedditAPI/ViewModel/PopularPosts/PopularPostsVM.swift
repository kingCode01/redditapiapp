//
//  PopularPostsVM.swift
//  RedditAPI
//
//  Created by Christian Hipolito on 7/25/19.
//  Copyright © 2019 Christian Hipolito. All rights reserved.
//

import Foundation
import Alamofire
import PromiseKit

enum RedditError: Error {
    case castFailed
}

class PopularPostsVM {
    
    var posts = [Post]()
    private var nextLink: String?
    
    func fetchPosts() -> Promise<Void>
    {
        if nextLink == nil {
            return fetchNewestPosts()
        }
        else {
            return fetchPostsWith(link: nextLink!)
        }
    }
    
    private func fetchNewestPosts() -> Promise<Void>
    {
        let dispatchQueue = DispatchQueue.init(label: "Fetch Popular Posts")
        
        return Promise<Void> { seal in
            Alamofire.request("http://www.reddit.com/.json").responseJSON(queue: dispatchQueue) { [weak self] (response) in
               self?.handle(response, with: seal)
            }
        }
    }
    
    private func fetchPostsWith(link: String) -> Promise<Void>
    {
        let dispatchQueue = DispatchQueue(label: "Fetch Popular Posts")
        
        return Promise<Void> { seal in
            Alamofire.request("https://www.reddit.com/.json?after=\(link)").responseJSON(queue: dispatchQueue) { [weak self] (response) in
               self?.handle(response, with: seal)
            }
        }
    }
    
    private func handle(_ response:DataResponse<Any>, with seal: Resolver<Void>)
    {
        if response.error == nil
        {
            guard let jsonResponse = response.result.value as? [String: Any] else {
                seal.reject(RedditError.castFailed)
                return
            }
            
            guard let jsonListing = jsonResponse["data"] as? [String: Any] else {
                seal.reject(RedditError.castFailed)
                return
            }
            
            nextLink = jsonListing["after"] as? String
            
            guard let jsonPosts = jsonListing["children"] as? [[String: Any]] else {
                seal.reject(RedditError.castFailed)
                return
            }
            
            let posts:[Post] = jsonPosts.compactMap {
                guard let dataDictionary = $0["data"] as? [String: Any] else {
                    return nil
                }
                return Post(json: dataDictionary)
            }
            
            self.posts.append(contentsOf: posts)
            
            seal.fulfill(())
        }
        else
        {
            seal.reject(response.error!)
        }
    }
}
