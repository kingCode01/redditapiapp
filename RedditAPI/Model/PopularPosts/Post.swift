//
//  Post.swift
//  RedditAPI
//
//  Created by Christian Hipolito on 7/25/19.
//  Copyright © 2019 Christian Hipolito. All rights reserved.
//

import Foundation

struct Post {
    let title: String?
    let imageURL: URL?
    
    init(json: [String: Any] ) {
        self.title = json["title"] as? String
        
        let imagesDictionary = ((json["preview"] as? [String:Any])?["images"] as? [[String: Any]])?.first
        
        let _640x426ImageDictionary = (imagesDictionary?["resolutions"] as? [[String:Any]])?[2]
        
        guard let _640x426ImageURLString = (_640x426ImageDictionary?["url"] as? String)?.replacingOccurrences(of: "&amp;", with: "&") else {
            self.imageURL = nil
            return
        }
        
        self.imageURL = URL(string: _640x426ImageURLString)
    }
}
